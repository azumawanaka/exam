$(function(){

    let memo = $("#memo"),
        btn_memo = $("#add_memo")

        btn_memo.on("click",function(e){
            e.preventDefault()
            memo.find("tbody").prepend('<tr>'
                    +'<td><input type="hidden" name="task_id[]" value=""><input type="checkbox" name="task_item_check[]" class="task_item_check" value="1"></td>'
                    +'<td><input type="text" placeholder="Task" class="form-control" name="task_title[]"></td>'
                    +'<td><select name="task_status[]" class="form-control"><option value="Pending">Pending</option><option value="Done">Done</option></td>'
                    +'<td><select name="task_priority[]" class="form-control"><option value="Low">Low</option><option value="Medium">Med</option><option value="High">High</option></select></td>'
                    +'<td><input type="date" placeholder="Due Date" class="form-control" name="task_ddate[]"></td>'
                    +'<td><input type="text" placeholder="Assignee" class="form-control" name="task_assignee[]"></td>'
                    +'<td><textarea name="task_notes[]" class="form-control" placeholder="Notes"></textarea></td>'
                    +'<td><a href="#" class="btn btn-danger close" data-attr=""><i class="fa fa-close"></i></a></td>'
                    +'</tr>'
                    )
            return false
        })

    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });
        

    $(document).on("click",".close",function(e){
        e.preventDefault()
        let id = $(this).attr("data-attr"),
            this_ = $(this)
        if(id != "") {

            if(confirm("Do you want to continue?")) {
                $.ajax({
                    type: "DELETE",
                    url: "forms/"+id,
                    data: {
                        "id": id,
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                    },
                    success: function(data) {
                        this_.parents("tr").remove()
                        var data_pr = JSON.parse(data)
                        if(data_pr.success) {
                            setTimeout(function(){
                                alert(data_pr.msg)
                            }, 500)
                        }
                    },
                    error: function(data) {
                        alert('Error'+ data);
                    }
                })
            }else{
                return false
            }
            
        }else{
            this_.parents("tr").remove()
        }

        return false
    })


    $(document).on("click","#check_all",function(e){
        if($(this).is(":checked")) {
            $(".task_item_check").trigger("click")
        }else{
            $("input[checked=checked]").trigger("click")
        }
    })

})