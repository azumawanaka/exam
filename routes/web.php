<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::delete('forms/{id}', 'FormsController@destroy')->name('forms.destroy');

Route::resource('forms', 'FormsController');

Auth::routes();

Route::get('/todo', 'FormsController@index')->name('todo');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/blog', 'BlogsController@index')->name('blog');
Route::get('/blog/new-post', 'BlogsController@new');

Route::put('forms', function () {
    if (preg_match('/^(.)\1*$/', $_REQUEST['abc'])) {
        return response("Response: 420 String-> ".json_encode($_REQUEST['abc']), 420)->header('Content-Type', 'text/plain');
    } else {
        return response("Response: 200: String-> ".json_encode($_REQUEST['abc']), 200)->header('Content-Type', 'text/plain');
    }
});

Route::put('forms/forms-reward', 'FormsController@get_reward_level');