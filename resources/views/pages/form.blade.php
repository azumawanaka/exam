{{-- @extends('main') --}}
@extends('layouts.app')
@section('title')
    Form
@endsection

@section('content')
    <section class="wrp">
        <div class="cntr">

            {{-- test no. 1 --}}
            <form action="/forms" method="POST" class="todo_form">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <label for="">
                    <input type="text" class="form-control" name="abc" value="">
                </label>
                <input type="submit" class="btn btn-primary" value="Save">
            </form>
            <form action="/forms-reward" method="POST" class="todo_form">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <label for="money1">
                    How much money they have spent with albumworks historically
                    <input type="number" class="form-control" id="money1" min="0" name="money1" value="" placeholder="Enter Money">
                </label>
                <label for="money2">
                    How much money they have spent in their most recent transaction.
                    <input type="number" class="form-control" id="money2" min="0" name="money2" value="" placeholder="Enter Money">
                </label>
                <input type="submit" class="btn btn-primary" value="Check Reward">
            </form>
            {{-- //test --}}

            
        </div>
    </section>
@endsection