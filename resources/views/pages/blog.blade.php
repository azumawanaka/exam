@extends('layouts.app')
@section('title')
    Blog
@endsection

@section('content')
    <section class="wrp">
        <div class="cntr">
            <div class="head-cntr dflex-ai mb-5">
                <h1 class="blog_main-title diblock">Posts</h1> <a href="#" class="btn btn-secondary btn-sm">Add New</a>
            </div>
            {{-- Posts Lists --}}
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="tbl-bloglist">
                    <thead>
                        <tr>
                            <th width="20px" scope="col">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="checkAll" name="checkAll" value="0">
                                    <label class="custom-control-label" for="checkAll"></label>
                                </div>
                            </th>
                            <th scope="col">
                                Title
                            </th>
                            <th scope="col">
                                Author
                            </th>
                            <th scope="col">
                                Categories
                            </th>
                            <th scope="col">
                                Tags
                            </th>
                            <th scope="col">
                                <i class="fa fa-comments"></i>
                            </th>
                            <th width="200px" scope="col">
                                Date
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="dblock mh-65">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="item[]" name="item[]" value="0">
                                    <label class="custom-control-label" for="item[]"></label>
                                </div>
                            </td>
                            <td class="opt_hover">
                                <a href="#">Hello World!</a>
                                <ul class="list-group list-group-horizontal-sm">
                                    <li class="list-group-item"><a href="#">Edit</a></li>
                                    <li class="list-group-item"><a href="#" class="text-danger">Trash</a></li>
                                    <li class="list-group-item"><a href="#">Preview</a></li>
                                </ul>
                            </td>
                            <td>
                                <a href="#">Filjumar</a>
                            </td>
                            <td>
                                <a href="#">Web</a>
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                2
                            </td>
                            <td class="text-muted">
                                <small>Last Modified Jan 01, 2020</small>
                            </td>
                        </tr>
                        <tr>
                            <td class="dblock mh-65">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="item[]" name="item[]" value="0">
                                    <label class="custom-control-label" for="item[]"></label>
                                </div>
                            </td>
                            <td class="opt_hover">
                                <a href="#">test</a>
                                <ul class="list-group list-group-horizontal-sm">
                                    <li class="list-group-item"><a href="#">Edit</a></li>
                                    <li class="list-group-item"><a href="#" class="text-danger">Trash</a></li>
                                    <li class="list-group-item"><a href="#">Preview</a></li>
                                </ul>
                            </td>
                            <td>
                                <a href="#">Filjumar</a>
                            </td>
                            <td>
                                <a href="#">Web</a>
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                2
                            </td>
                            <td class="text-muted">
                                <small>Last Modified Jan 01, 2020</small>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            {{-- //Posts --}}
        </div>
    </section>
@endsection