@extends('layouts.app')
@section('title')
    Add New Post
@endsection

@section('content')
    <section class="wrp">
        <div class="container">
            <div class="head-cntr dflex-ai mb-3">
                <h1 class="blog_main-title diblock">Add New Post</h1>
            </div>
            <div class="row">
                <div class="col-md-9 col-sm-7 col-xs-12">
                    <form action="/blog" class="form" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Add Title">
                        </div>
                        <div class="form-group">
                            <div id="summernote"></div>
                            <div class="placeholder">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3 col-sm-5 col-xs-12">
                    <div class="card p-3">
                        <div class="dflex-ai jc-sb">
                            <button type="submit" class="btn btn-secondary btn-sm">Publish</button>
                            <button type="button" class="btn btn-primary btn-sm">Preview</button>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Cras justo odio</li>
                            <li class="list-group-item">Dapibus ac facilisis in</li>
                            <li class="list-group-item">Vestibulum at eros</li>
                        </ul>
                        <div class="card-body">
                            <a href="#" class="btn btn-link">Card link</a>
                            <a href="#" class="tn btn-link">Another link</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection