<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('title', 250)->nullable();
            $table->longText('content')->nullable();
            $table->char('category', 250)->nullable();
            $table->char('tags', 250)->nullable();
            $table->integer('comments', false, false)->length(12)->default(0);
            $table->char('author', 150)->nullable();
            // $table->char('profile', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
